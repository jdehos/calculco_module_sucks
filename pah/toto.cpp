#include <iostream>
#include <armadillo>

using namespace arma;

void toto() {
    // read data from csv file
    mat data;
    data.load("values.csv", csv_ascii);
    data = data.t();
    cout << data.t() << endl;
    cout << "data size : " << data.n_rows << " , " << data.n_cols << endl;


    // model the data as a diagonal GMM with 2 Gaussians

    gmm_diag model;

    bool status = model.learn(data, 3, eucl_dist, static_subset, 10, 10, 1e-0, true);

    if(status == false)
      {
      cout << "learning failed" << endl;
      }

    model.hefts.print("weights:");
    model.means.print("means:");
    model.dcovs.print("dcovs:");

}

