```
ml purge

mkdir -p ~/tmp
export MYPATH=$HOME/opt/module_sucks
mkdir -p $MYPATH/modules
cd $MYPATH/modules
wget https://gogs.univ-littoral.fr/jdehos/calculco_armadillo/src/master/module_sucks
module use $MYPATH/modules
module load module_sucks 

cd ~/tmp
wget http://github.com/xianyi/OpenBLAS/archive/v0.2.20.tar.gz
tar zxf v0.2.20.tar.gz
cd OpenBLAS-0.2.20/
make -j16 DYNAMIC_ARCH=1 NO_LAPACK=0 NO_LAPACKE=0 NO_SHARED=1
make PREFIX=$MYPATH install

cd ~/tmp
wget https://www-lisic.univ-littoral.fr/~dehos/pub/armadillo-7.960.1.tar.gz
tar zxf armadillo-7.960.1.tar.gz
cd armadillo-7.960.1/
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=$MYPATH -Dopenblas_LIBRARY=$MYPATH/lib -DLAPACK_LIBRARY="$MYPATH/lib" -DBLAS_LIBRARY="$MYPATH/lib" ..
make -j16
make install

cd ~/tmp
wget https://github.com/opencv/opencv/archive/3.3.0.tar.gz
tar zxf 3.3.0.tar.gz 
cd opencv-3.3.0/
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=$MYPATH -DOpenBLAS_INCLUDE_DIR=$MYPATH/include -DOpenBLAS_LIB=$MYPATH/lib/libopenblas.so ..
make -j16
make install
```

